# Visual pattern preferences

This repository contains  
- the code to reproduce the stimuli -> Folder: StimulusCreationScripts
- the images used for the online experiment in high resolution -> Folder: Experiment_Images
- the preprocessed data -> Folder: Data
- the code to reproduce the main analyses and figures -> Script: VisualPatternAnalyses_OSF.R
